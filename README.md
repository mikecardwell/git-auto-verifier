# Git Auto Verifier

I've started PGP signing my GIT commits. I wanted GIT to automatically verify
the HEAD commit after running certain commands, but found no options to do
this.

As a solution, I wrote a wrapper script which does this. At the moment, it
runs after you do any one of git: checkout, clone, commit, merge, pull, rebase
or status.

## Example:

```shell
$ mkdir test-project && cd test-project
$ git init
Initialized empty Git repository in /tmp/test-project/.git/
$ date > TEST_FILE
$ git add TEST_FILE 
$ git commit -m "Unsigned commit"
[master (root-commit) ff6ef9d] Unsigned commit
 1 file changed, 1 insertion(+)
 create mode 100644 TEST_FILE
! GPG - HEAD is not signed !
$ git status
On branch master
nothing to commit, working tree clean
! GPG - HEAD is not signed !
$ date > TEST_FILE
$ git add TEST_FILE 
$ git commit -S -m "Signed commit"
[master ad3c06d] Signed commit
 1 file changed, 1 insertion(+), 1 deletion(-)
* GPG - HEAD is signed by me *
$ git status
On branch master
nothing to commit, working tree clean
* GPG - HEAD is signed by me *
```

The lines that this script adds above, are:

- ! GPG - HEAD is not signed !
- * GPG - HEAD is signed by me *

There is another line that could appear, which is not in my example above:

- ! GPG - HEAD is signed by somebody else !

## Install

You need to have git and gpg installed obviously. You then need to place the
`git-auto-verifier.sh` bash script somewhere in your PATH named "git", earlier
than the real git binary.

For example, my real git binary is at `/usr/bin/git`, and the `git-auto-verifier.sh`
script is at `/home/mike/bin/git`. And the reason this works is because
`/home/mike/bin/git` comes before `/usr/bin/` in my PATH environment variable:

```shell
$ echo "$PATH"
/home/mike/bin:/usr/local/bin:/usr/bin:/bin
```