#!/usr/bin/env bash

VERIFY_COMMANDS="^(checkout|clone|commit|merge|pull|rebase|status)$"

## Figure out where the real git binary is

THIS_GIT_1="$0"
THIS_GIT_2="$(realpath "$0")"
REAL_GIT=""

for P in $(echo "$PATH" | sed 's/:/ /g'); do
    if [[ "$P/git" != "$THIS_GIT_1" && "$P/git" != "$THIS_GIT_2" && -f "$P/git" ]]; then
        REAL_GIT="$P/git"
        break
    fi
done

if [ "$REAL_GIT" == "" ]; then
    echo "Failed to locate the real git binary" >&2
    exit 1
fi

## Proxy through the git command. If it's a clone, then we need to
## capture STDERR to obtain the clone directory, and then cd into it

if [ "$1" == "clone" ]; then
    TMPFILE=$(mktemp --suffix=-gitwrapper)
    if [ "$?" != "0" ]; then exit $?; fi
    $REAL_GIT "$@" 2> >(tee "$TMPFILE" >&2)
    EXIT_CODE="$?"
    STDERR="$(cat "$TMPFILE")"
    rm -f "$TMPFILE"
    if [ "$EXIT_CODE" != 0 ]; then exit $EXIT_CODE; fi

    CLONE_PATH="$(echo "$STDERR" | grep '^Cloning into ' | head -n 1 | sed -E 's/^Cloning into .//' | sed -E 's/[^.]\.+//')"
    cd "$CLONE_PATH"
    if [ "$?" != "0" ]; then exit $?; fi
else
    $REAL_GIT "$@"
    if [ "$?" != "0" ]; then exit $?; fi
fi

## GPG Verify the HEAD after running certain commands

if [[ "$1" =~ $VERIFY_COMMANDS ]]; then
    SIGNED_BY_KEY=$($REAL_GIT verify-commit HEAD --raw 2>&1 | grep VALIDSIG | head -n 1 | sed -E 's/.+ //')
    if [ "$SIGNED_BY_KEY" == "" ]; then
        echo -e "\033[0;31m! GPG - HEAD is not signed !\033[0m" >&2
    else
        for FP in $(gpg --list-secret-keys --with-colons | grep ^fpr: | awk -F ':' {'print $10'}); do
            if [ "$SIGNED_BY_KEY" == "$FP" ]; then
                echo -e "\033[0;32m* GPG - HEAD is signed by me *\033[0m" >&2
                exit 0
            fi
        done
        echo -e "\033[0;31m! GPG - HEAD is signed by somebody else !\033[0m" >&2
    fi
fi
